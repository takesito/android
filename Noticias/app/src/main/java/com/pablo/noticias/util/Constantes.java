package com.pablo.noticias.util;

/**
 * Created by Pablo on 24/10/2017.
 */

public class Constantes {
    public static final String TABLA_NOTICIAS = "noticias";
    public static final String TITULO = "titulo";
    public static final String AUTOR = "autor";
    public static final String TEXTO = "texto";
    public static final String FECHA = "fecha";
}
