package com.pablo.noticias.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.pablo.noticias.util.Constantes.AUTOR;
import static com.pablo.noticias.util.Constantes.FECHA;

import static com.pablo.noticias.util.Constantes.TABLA_NOTICIAS;
import static com.pablo.noticias.util.Constantes.TEXTO;
import static com.pablo.noticias.util.Constantes.TITULO;

/**
 * Created by Pablo on 24/10/2017.
 */

public class BaseDatos extends SQLiteOpenHelper {

    private static final String NOMBRE = "noticias.db";
    private static final int VERSION = 1;

    public BaseDatos(Context context) {
        super(context, NOMBRE, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + TABLA_NOTICIAS + "(" + "id INT PRIMARY KEY AUTOINCREMENT," + TITULO + " TEXT, " + TEXTO + " TEXT, " + AUTOR + " TEXT, " + FECHA + " TEXT)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE " + TABLA_NOTICIAS);

    }
}
