package com.pablo.listado;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class AnadirActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anadir);

        Button btanadir = (Button) findViewById(R.id.btAnadir);
        btanadir.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        EditText etMensaje = (EditText) findViewById(R.id.etMensaje);
        String mensaje = etMensaje.getText().toString();
        MainActivity.listaMensajes.add(mensaje);

        etMensaje.setText("");


    }
}
